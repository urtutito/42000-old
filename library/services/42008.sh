comand_selector () {

    if [ -z $command_selected ]; then
                
        read -p "  » " command_selected

    fi

    case $command_selected in

        start|st )

            /ant/42008/csgoserver start
            ;;

        stop|sp )

            /ant/42008/csgoserver stop
            ;;

        restart|r )

            /ant/42008/csgoserver restart
            ;;

        details|dt )

            /ant/42008/csgoserver details
            ;;

        update|u )

            /ant/42008/csgoserver details
            ;;

        update-lgsm|ul )

            /ant/42008/csgoserver details
            ;;

        backup|b )

            /ant/42008/csgoserver details
            ;;
                        
        console|c )

            /ant/42008/csgoserver details
            ;;

        debug|d )

            /ant/42008/csgoserver details
            ;;

        * )

            echo " Invalid command, please try again. "
            command_selected=""
            comand_selector

    esac

}

comand_selector